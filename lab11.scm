;true
(define true (lambda (a b) a))
;false
(define false (lambda (a b) b))	
;OR
(define OR (lambda (m n) (lambda(a b) (n a (m a b)))))
;AND
(define AND (lambda(m n a b)(n (m a b) b)))
;isZero
(define isZero (lambda (n) (n (lambda(x) (false)) true)))
;add 
(define add (lambda (m n s z) (m s(n s z))))
;pred
(define pred (lambda (n) (n (lambda (x y) (x 1) (lambda (u) (add (x y) 1 ) y)) (lambda (v) 0) 0)))
;sub
(define sub(lambda(m n)(m pred n)))
;LEQ
(define LEQ (lambda (m n)(IsZero (sub m n))))
;EQ
(define EQ (lambda (m n) (AND (LEQ (m n)) (LEQ (n m))))) 
;LT
(define LT (lambda (a b) ((AND ((NOT (EQ a b)) (LEQ a b))))))
;func
(define funct (lambda (f n) (EQ (n 0)) 0 (add (f n-1))))
;Y-combinator
(define Y (lambda (f) ((lambda(x)f(x x)) (lambda(x)f(x x)))))
;uselessfunction
(define UselessFunction (lambda (m n) (OR((isZero m) (LT m n)) (Y funct(n funct)) (add m n))))